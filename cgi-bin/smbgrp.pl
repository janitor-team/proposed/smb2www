#! /usr/bin/perl

# SMB2WWW - a smb to WWW gateway; access windows computers through a browser
# Copyright (C) 1997 Remco van Mook

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

# The author can be contacted by e-mail: remco@samba.anu.edu.au

use strict;
use smb2www;

my %all=decode_query;
my $key = "";
foreach $key (keys %all) {
  $all{$key} = urlDecode $all{$key};
}

my @smbout = GetSMBHosts ("$all{group}","$all{master}");

if ( $#smbout < 0 ) {
   header ("$text{no_group}");
   print "<H1>$text{no_group2}</H1><BR>\n";
   table ( 
     href (shref("all"),image ($cfg{icon_all},"$text{network}")),
     "<H3>$text{ent_net}</H3>",
     ""
   );
   trailer;
   exit;
}

header ( "$text{in_group} $all{'group'}" );

table ( 
  href (shref("all"), image ($cfg{icon_all},"$text{network}")),
  "",
  "",
  "<H3>$text{ent_net}</H3>",
  ""
);        
table ( 
  "",
  image ($cfg{icon_group},"$text{workgroup}"),
  "",
  "<H3>".uc ($all{'group'})."</H3>",
  ""
);

my $line = "";
foreach $line ( @smbout ) { 
    my $url = shref("share","$all{'group'}","$all{'master'}","$line->{name}");
    table ( "",
            "",
            href ($url,image ($cfg{icon_computer},"$text{shares}")),
            href ($url,"<B>$line->{name}</B>"),
            "$line->{comment}"
    );
  }                                                                             
trailer;
