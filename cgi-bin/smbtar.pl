#! /usr/bin/perl

# SMB2WWW - a smb to WWW gateway; access windows computers through a browser
# Copyright (C) 1997 Remco van Mook

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

# The author can be contacted by e-mail: remco@samba.anu.edu.au

use smb2www;
use strict;

select STDOUT; $| = 1;
$ENV{'USER'}=$cfg{username};

my %all=decode_query;

my $info = $ENV{'PATH_INFO'};
if ($info =~ /\/([\w\s\$\#\!\-\_\%\'\.]+)\/([\w\s\$\#\!\-\_\%\'\.]+)\/(.*)/) {
   $all{host} = $1;
   $all{share} = $2;
   $all{dir} = $3;
}
my $key = "";
foreach $key (keys %all) {
  $all{$key} = urlDecode $all{$key};
}

if ($all{auth} ne "") {
  ($all{user},$all{pass}) = GetAuth ("$all{auth}");
}

my @dirt = split (/\//, $all{dir});
my $file = pop @dirt;
my $dir = "\\".join ('\\',@dirt);

print "Cache-Control: no-cache\n";
print "Content-type: application/x-tar\n\n";

GetSMBTar ("$all{host}","$all{share}","$dir","$all{user}","$all{pass}");

